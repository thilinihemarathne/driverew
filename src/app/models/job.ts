export class Job {

    constructor(
        public jobNumber: string,
        public registrationNumber: string,
        public isTradePlateRequired: boolean,
        public vehicleClass: string,
        public collectionDate: Date,
        public collectionTime: Date,
        public collectionPostCode: Date,
        public deliveryDate: Date,
        public deliveryTime: Date,
        public deliveryPostCode: string,
        public returnRegistrationNumber: string,
        public returnVehicleClass: string,
        public returnVehiclePostCode: string,

      ) {  }
}
