import { Component, OnDestroy, OnInit, } from '@angular/core';

declare var $:any;
@Component({
  selector: 'app-driver-form',
  templateUrl: './driver-form.component.html',
  styleUrls: ['./driver-form.component.css']
})
export class DriverFormComponent implements OnDestroy, OnInit {

  public nav:any;

  public constructor() {
    //this.nav = document.querySelector('nav.navbar');
    //console.log(this.nav);
  }

  public ngOnInit():any {
    //this.nav.className += " white-bg";
    $(document).ready(function() {
        $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });
 });


  }
  
  
  public ngOnDestroy():any {
    //this.nav.classList.remove("white-bg");
  }

}
