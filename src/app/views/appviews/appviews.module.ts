import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";

import {StarterViewComponent} from "./starterview.component";
import {LoginComponent} from "./login.component";
import {TestViewComponent} from "./test.component";

import {PeityModule } from '../../components/charts/peity';
import {SparklineModule } from '../../components/charts/sparkline';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    StarterViewComponent,
    TestViewComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    PeityModule,
    SparklineModule,
    FormsModule,
  ],
  exports: [
    StarterViewComponent,
    TestViewComponent,
    LoginComponent,
  ],
})

export class AppviewsModule {
}
